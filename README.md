# Booksite

This is a repository managing the website assets for a one-page site that houses some weird SF books by this one guy.

## Is this useful?

This is probably not terribly useful to you, whomever you are. It's just a bunch of random html and css and js files for a little indie author's website. You're welcome to use it, but I'm just warning you that it's not, like, you know, a code release. It's just a random stash of code on the internet.